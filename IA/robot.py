import random

ROBOT_MOV, AGENT_MOV, FREE_CELL = 2, 1, 0


class Robot():
    def __init__(self):
        self.cell1 = 0
        self.cell2 = 0
        self.cell3 = 0
        self.cell4 = 0
        self.cell5 = 0
        self.cell6 = 0
        self.cell7 = 0
        self.cell8 = 0
        self.cell9 = 0
        self.memory = [self.cell1, self.cell2, self.cell3,
                       self.cell4, self.cell5, self.cell6,
                       self.cell7, self.cell8, self.cell9]

    @property
    def memory(self):
        return self._memory

    @memory.setter
    def memory(self, frame):
        self._memory = frame

    def check_env(self, tictactoe):
        self.memory = tictactoe

    def play(self):
        free_cells = self.free_cells()
        robot_plays = self.robot_plays()
        agent_plays = self.agent_plays()

        "if first playng it means all cells its free"
        if len(free_cells) == 9:
            position_played = random.sample([1, 3, 7, 9], 1).pop()
            self.update_memory(position_played)
            return position_played

        "try not to lose"
        for mov1 in agent_plays:
            for mov2 in agent_plays:
                if mov1 == mov2:
                    continue
                else:
                    free_cell = self.third_free_on_the_line(mov1, mov2)
                    if free_cell:
                        self.update_memory(free_cell)
                        return free_cell

        "play to fill the trird cell and win"
        for mov1 in robot_plays:
            for mov2 in robot_plays:
                if mov1 == mov2:
                    continue
                else:
                    free_cell = self.third_free_on_the_line(mov1, mov2)
                    if free_cell:
                        self.update_memory(free_cell)
                        return free_cell



        "best movement"
        for played in robot_plays:
            if played == 1 and any(
                    cel in free_cells for cel in [3, 7, 9]):
                possible_plays = list(
                    set(free_cells).intersection([3, 7, 9])
                )
                cell = random.sample(possible_plays, 1).pop()
                self.update_memory(cell)
                return cell

            elif played == 3 and any(
                    cel in free_cells for cel in [1, 7, 9]):
                possible_plays = list(
                    set(free_cells).intersection([1, 7, 9])
                )
                cell = random.sample(possible_plays, 1).pop()
                self.update_memory(cell)
                return cell

            elif played == 7 and any(
                    cel in free_cells for cel in [1, 3, 9]):
                possible_plays = list(
                    set(free_cells).intersection([1, 3, 9])
                )
                cell = random.sample(possible_plays, 1).pop()
                self.update_memory(cell)
                return cell

            elif played == 9 and any(
                    cel in free_cells for cel in [1, 3, 7]):
                possible_plays = list(
                    set(free_cells).intersection([1, 3, 7])
                )
                cell = random.sample(possible_plays, 1).pop()
                self.update_memory(cell)
                return cell

        "any movement"
        cell = random.sample(free_cells, 1).pop()
        self.update_memory(cell)
        return cell

    def draw_mov(self, position):
        if position == 1:
            return 0, 0
        elif position == 2:
            return 0, 1
        elif position == 3:
            return 0, 2
        elif position == 4:
            return 1, 0
        elif position == 5:
            return 1, 1
        elif position == 6:
            return 1, 2
        elif position == 7:
            return 2, 0
        elif position == 8:
            return 2, 1
        elif position == 9:
            return 2, 2

    def third_free_on_the_line(self, first, second):
        free_cells = self.free_cells()
        if all(cell in [1, 2, 3] for cell in [first, second]):
            if 1 in free_cells:
                return 1
            elif 2 in free_cells:
                return 2
            elif 3 in free_cells:
                return 3
        elif all(cell in [4, 5, 6] for cell in [first, second]):
            if 4 in free_cells:
                return 4
            elif 5 in free_cells:
                return 5
            elif 6 in free_cells:
                return 6
        elif all(cell in [7, 8, 9] for cell in [first, second]):
            if 7 in free_cells:
                return 7
            elif 8 in free_cells:
                return 8
            elif 9 in free_cells:
                return 9
        elif all(cell in [1, 4, 7] for cell in [first, second]):
            if 1 in free_cells:
                return 1
            elif 4 in free_cells:
                return 4
            elif 7 in free_cells:
                return 7
        elif all(cell in [2, 5, 8] for cell in [first, second]):
            if 2 in free_cells:
                return 2
            elif 5 in free_cells:
                return 5
            elif 8 in free_cells:
                return 8
        elif all(cell in [3, 6, 9] for cell in [first, second]):
            if 3 in free_cells:
                return 3
            elif 6 in free_cells:
                return 6
            elif 9 in free_cells:
                return 9
        elif all(cell in [7, 5, 3] for cell in [first, second]):
            if 7 in free_cells:
                return 7
            elif 5 in free_cells:
                return 5
            elif 3 in free_cells:
                return 3
        elif all(cell in [1, 5, 9] for cell in [first, second]):
            if 1 in free_cells:
                return 1
            elif 5 in free_cells:
                return 5
            elif 9 in free_cells:
                return 9
        else:
            return False

    def robot_plays(self):
        robot_move_on = []
        for i, cell in enumerate(self.memory):
            if cell == ROBOT_MOV:
                robot_move_on.append(i)
        return robot_move_on

    def free_cells(self):
        free_cells = []
        for i, cell in enumerate(self.memory):
            if cell == FREE_CELL:
                free_cells.append(i + 1)
        return free_cells

    def agent_plays(self):
        agent_move_on = []
        for i, cell in enumerate(self.memory):
            if cell == AGENT_MOV:
                agent_move_on.append(i + 1)
        return agent_move_on

    def update_memory(self, position_played):
        self.memory[position_played - 1] = ROBOT_MOV


if __name__ == '__main__':
    pass